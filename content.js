const styleSheet = createStyleTag();

if (styleSheet) {
  styleSheet.insertRule('#tinymce {max-width: none}');
  styleSheet.insertRule('#tinymce {font-family: "Open Sans", sans-serif;}');
  styleSheet.insertRule('#tinymce p {font-family: "Open Sans", sans-serif;}');
  styleSheet.insertRule('#tinymce img {max-width: 100%}');
}

function createStyleTag() {
  const iframe = document.querySelector('#content_ifr');
  const iframeDoc = iframe.contentDocument;
  const iframeHead = iframeDoc && iframeDoc.head;
  const style = iframeDoc.createElement('STYLE');

  style.type = 'text/css';

  if (iframeHead) {
    iframeHead.insertAdjacentHTML('afterbegin', '<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">');
    iframeHead.appendChild(style);
  }

  return style.sheet;
}